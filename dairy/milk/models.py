from django.db import models
from django.contrib.auth.models import User
from tinymce.models import HTMLField


# Create your models here.
class Product(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField()
    user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return str(self.quantity)
