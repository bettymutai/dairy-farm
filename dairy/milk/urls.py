from django.conf.urls import url
from . import views
from milk.views import BarView


urlpatterns=[
    url('^$',views.milk_today,name = 'milkToday'),
    url(r'^archives/(\d{4}-\d{2}-\d{2})/$',views.past_days_milk,name = 'pastMilk'),
    url(r'^new/product$', views.new_product, name='new-product'),
    url(regex='^bar/$', view=BarView.as_view(), name='bar')

]
