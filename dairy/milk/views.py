from django.shortcuts import render,redirect
from django.http import HttpResponse,Http404,HttpResponseRedirect
from .models import Product
from django.contrib.auth.decorators import login_required
from .forms import NewProductForm
from django.urls import reverse
from highcharts.views import (HighChartsMultiAxesView, HighChartsPieView,
                              HighChartsSpeedometerView, HighChartsHeatMapView, HighChartsPolarView)
import datetime as dt



# Create your views here.
@login_required(login_url='/accounts/login')
def new_product(request):
    user = request.user
    if request.method == 'POST':
        form = NewProductForm(request.POST, request.FILES)
        if form.is_valid():
            product = form.save(commit=False)
            product.user = user
            product.save()
            return HttpResponseRedirect('/')

    else:
        form = NewProductForm()
    return render(request, 'new_product.html', {"form": form})

def milk_today(request):
    date = dt.date.today()
    product_list = Product.objects.all()
    return render(request, 'all-milk/today-milk.html', {"product_list": product_list})

def past_days_milk(request,past_date):
    try:
        # Converts data from the string Url
        date = dt.datetime.strptime(past_date,'%Y-%m-%d').date()

    except ValueError:
        #Raise 404 error when ValueError is thrown
        raise Http404()
        assert False

    if date == dt.date.today():
        return redirect(milk_today)

    return render(request, 'all-milk/past-milk.html', {"date": date})


class ChartData(object):
    def check_valve_data():
        data = {'quantity': []}

        product_list = Product.objects.all()

        for product in product_list:
            data['quantity'].append(product.quantity)

        return data

class BarView(HighChartsMultiAxesView):
    title = 'My Dairy Stats'
    categories = ['Milk']
    chart = {'zoomType': 'xy'}
    tooltip = {'shared': 'true'}
    legend = {'layout': 'vertical', 'align': 'right',
              'floating': 'true', 'verticalAlign': 'middle',
              'y': 10, 'borderColor': '#e3e3e3'}

    @property
    def yaxis(self):
        y_axis = [
            {},
            {'gridLineWidth': 5,'title': {'text': "Milk", 'style': {'color': '#3771c8'}},
             'labels': {'style': {'color': '#3771c8'}, 'format': '{value} litres'}},
            {}
        ]
        return y_axis

    @property
    def series(self):
        data = ChartData.check_valve_data()
        series = [
            {
                'name': 'Milk',
                'type': 'spline',
                'yAxis': 2,
                'data': data['quantity'],
                'marker': { 'enabled': 'true' },
                'dashStyle': 'shortdot',
                'color': '#666666',
                }
        ]
        return series
